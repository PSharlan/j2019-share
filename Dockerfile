FROM openjdk:8

COPY ./target/collections.manager-0.0.1-SNAPSHOT.jar manager.jar

CMD ["java" , "-jar" , "manager.jar"]