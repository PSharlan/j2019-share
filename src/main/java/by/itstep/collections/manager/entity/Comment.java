package by.itstep.collections.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "message")
    private String message;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user; // Тот кто его оставил

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne // Множество item'ов у каждой коллекции
    @JoinColumn(name = "collection_id")
    private Collection collection; // К чему относится

    @Column(name = "created_at")
    private Date createdAt; // Дата создания коммента
}
